## QA Challenge ##

### Part I - Test Specification ###

#### Application ####

Reev is a sales engagement platform that turns salespeople into superheroes.
Suppose our developers are starting a new version of the platform and the first thing they need to ship is the login page, illustrated by the image below:

![Login](login.png)

#### Instructions ####

Your job is to specify all relevant test scenarios for our login page.
There are no particular rules on how to write the scenarios. Just be sure to describe them clearly and easily understandable.

#### Tools ####

Just add your test scenarios in a plain text file or a README.md file.
It is not necessary to use complementary tools for this part of the test.

### Part II - Test Automation ###

#### Application ####

[Automation Practice](http://automationpractice.com/) is a sample ecommerce website designed to allow the practice of automated testing.

It works just like a real e-commerce platform, with different workflows for adding products to cart, checking out cart, performing payments, etc.

The website was an inspiration from the fact that there is no website that can bridge the gaps between the differences among various programming languages and help non-programmers get a taste of automation.

You can use it as your sandbox to put in practice all you know about automated tests.

#### Instructions ####

Your job is to write automated end-to-end tests for the [Automation Practice](http://automationpractice.com/) website.

You do not need to automate every single user interaction, but make sure that the most important workflows will be covered by your automation.

#### Tools ####

You are free to use any tools, frameworks and programming languages you want.

Just make sure you provide detailed guidelines on how to execute everything properly.

## Delivery ##

Create a new Bitbucket repository to version your code.

When you are done, send your repository URL to the recruiter that is guiding your process.

If your repository is private, don't forget to give the user **outbound-marketing** read access to it.

It is highly recommended to perform smaller and descriptive commits. This will make it easier to assess your progress.

You will have 4 days, counting from the day that you received this, to complete the challenge.

## What will be evaluated? ##

- Test planning capabilities
- Testing decisions
- Automation skills
- Technological choices
- Code organization
- Design patterns usage
- Git usage

## What will not be evaluated? ##

- Completion time

Feel free to use all the available time to deliver the best you can :)
